
A set of various useful tools and custom libraries for working with databases.

Compatibility
-------------

**Python**: 3.7, 3.8, 3.9

Summary
-------

 - **functions.py** - functions for working with tables, extensions etc.
 - **roles.py** - functions for working with roles and users
 - **services.py** - database services
 - **settings.py** - default database settings (example)

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

.. attention::

    Docker engine is required because all the tests use docker sdk
    and postgres docker container (obviously).


tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

`pyenv local 3.7.5 3.8.1 3.9.0`

Then you can run `tox` command to test against all of them.

coverage
^^^^^^^^

Don't use PyCharm bundled coverage. Because it doesn't
ignore module docstrings you get wrong results.

Documentation
-------------

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
