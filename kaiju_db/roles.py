"""
User and user role functions for postgresql.

Usually these functions are being invoked by a :class:`.services.DatabaseService`
on its start and not intended to be used directly.

Functions
---------

"""

from asyncpgsa.connection import SAConnection

from collections import namedtuple


__all__ = [
    'check_role_exists', 'create_user',
    'create_r_role', 'create_rw_role', 'create_rwx_role', 'create_rx_role',
    'ROLES', 'Role', 'role_r', 'role_rw', 'role_rwx', 'role_rx'
]


async def drop_role(connection: SAConnection, name: str):
    """
    Delete a role.
    """

    async with connection.transaction():
        await connection.execute(f'DROP OWNED BY {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM {name};')
        await connection.execute(f'DROP ROLE {name};')


async def _create_basic_role(connection: SAConnection, db_name: str, name: str, password: str):
    await connection.execute(f"CREATE ROLE {name} WITH LOGIN PASSWORD '{password}';")
    await connection.execute(f"GRANT CONNECT ON DATABASE {db_name} TO {name};")
    await connection.execute(f'GRANT USAGE ON SCHEMA public TO {name};')


async def create_r_role(connection: SAConnection, db_name: str, name: str, password: str):
    """
    Create a new role with readonly access.
    """

    async with connection.transaction():
        await _create_basic_role(connection, db_name, name, password)
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO {name};')


async def create_rx_role(connection: SAConnection, db_name: str, name: str, password: str):
    """
    Create a new role with read and function permissions.
    """

    async with connection.transaction():
        await _create_basic_role(connection, db_name, name, password)
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO {name};')


async def create_rw_role(connection: SAConnection, db_name: str, name: str, password: str):
    """
    Create a new role with read and write access.
    """

    async with connection.transaction():
        await _create_basic_role(connection, db_name, name, password)
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO {name};')


async def create_rwx_role(connection: SAConnection, db_name: str, name: str, password: str):
    """
    Create a new role with full access rights.
    """

    async with connection.transaction():
        await _create_basic_role(connection, db_name, name, password)
        await connection.execute(f'GRANT ALL PRIVILEGES ON SCHEMA public TO {name};')
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO {name};')


async def check_role_exists(connection: SAConnection, role: str) -> bool:
    """
    Checks if role with such name exists in the database.
    """

    result = await connection.fetchval(f"SELECT 1 FROM pg_roles WHERE rolname='{role}';")
    if result is None:
        return False
    return result == 1

# все группы пользователей

Role = namedtuple('db_role', 'name, init_script, comment')
role_r = Role('r', create_r_role, 'readonly user')
role_rw = Role('rw', create_rw_role, 'normal user')
role_rx = Role('rx', create_rx_role, 'user for db testing')
role_rwx = Role('rwx', create_rwx_role, 'root user')
ROLES = (role_r, role_rw, role_rx, role_rwx)


async def create_user(connection: SAConnection, db_name: str, user: str, password: str, role: str):
    """
    Create a new user with a specific role.

    :param connection:
    :param db_name: database
    :param user: user name
    :param password: user password
    :param role: role type (r, rw, rwx)
    """

    if not isinstance(role, Role):
        try:
            role = next(r for r in ROLES if r.name == role)
        except StopIteration:
            raise KeyError(
                'роли от которой наследуется новый пользователь не существует:'
                ' user=%s, role=%s' % (user, role)) from None

    return await role.init_script(connection, db_name, user, password)
