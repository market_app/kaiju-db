"""
Database settings mapping for a database service.

.. code-block:: python

    DB = {
        'settings': {
            'tablespace': '',
            'user': 'default',  # database user tag (see "users")
        },
        'pool': {            # asyncpg pool connection settings
            'host': 'localhost',
            'port': 5432,
            'database': 'engine',
            'min_size': 4,
            'max_size': 64,
            'max_queries': 100000,
            'max_inactive_connection_lifetime': 30
        },
        'users': {          # user data
            'root': {       # root user must be present and must be a superuser/admin
                'credentials': {    # root user and password
                    'user': 'postgres',
                    'password': 'postgres'
                },
                'role': 'rwx',      # not used
                'comment': 'рутовый юзер, которому доступны все действия,'
                           ' включая создание и удаление таблиц, данный юзер'
                           ' должен быть создан вручную'
            },
            'default': {    # default user should be present (or you may change "settings.user" to another value)
                'credentials': {    # default user credentials
                    'user': 'engine_default',
                    'password': 'engine_default'
                },
                'role': 'rw',   # role defines which rights the user will have
                                # r - readonly
                                # rx - read and execute functions
                                # rwx - read, write, execute
                'comment': 'пользователь для обычных операций с базой во время работы'
                           ' приложения (создание, редактирование, чтение записей)'
            }
        },
        'extensions': ["uuid-ossp"]  # enabled postgres extensions
    }  #: default database settings


"""

DB = {
    'init_db': True,
    'settings': {
        'tablespace': '',
        'user': 'default',  # database user tag (see "users")
    },
    'pool': {            # asyncpg pool connection settings
        'host': 'localhost',
        'port': 5432,
        'database': 'engine',
        'min_size': 4,
        'max_size': 64,
        'max_queries': 100000,
        'max_inactive_connection_lifetime': 30
    },
    'users': {          # user data
        'root': {       # root user must be present and must be a superuser/admin
            'credentials': {    # root user and password
                'user': 'postgres',
                'password': 'postgres'
            },
            'role': 'rwx',      # not used
            'comment': 'рутовый юзер, которому доступны все действия,'
                       ' включая создание и удаление таблиц, данный юзер'
                       ' должен быть создан вручную'
        },
        'default': {    # default user should be present (or you may change "settings.user" to another value)
            'credentials': {    # default user credentials
                'user': 'engine_default',
                'password': 'engine_default'
            },
            'role': 'rw',   # role defines which rights the user will have
                            # r - readonly
                            # rx - read and execute functions
                            # rwx - read, write, execute
            'comment': 'пользователь для обычных операций с базой во время работы'
                       ' приложения (создание, редактирование, чтение записей)'
        }
    },
    'extensions': ["uuid-ossp"]
}
