"""
Various functions for working with databases. Usually they are used by
a :class:`.services.DatabaseService` on init.

Functions
---------

"""

import io
import functools
from typing import List

import sqlalchemy as sa
from asyncpgsa.connection import SAConnection
from asyncpg.exceptions import UndefinedTableError, UndefinedColumnError

__all__ = (
    'dump_sql', 'check_db_exists', 'check_table_exists', 'check_table_health',
    'create_db', 'create_table', 'create_extensions',
)


def dump_sql(func, bind=False):
    """
    This function is used to convert an sqlalchemy SQL structure to an actual
    SQL.
    """

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        out = io.StringIO()

        # noinspection PyUnusedLocal
        def dump(sql, *multiparams, **params):
            out.write('{};\n'.format(str(sql.compile(dialect=dump.dialect)).strip()))

        engine = sa.create_engine('postgres://', strategy='mock', executor=dump)
        dump.dialect = engine.dialect

        if bind:
            func(*args, bind=engine, **kwargs)
        else:
            func(engine, *args, **kwargs)

        return out.getvalue()
    return func_wrapper


async def check_table_exists(connection: SAConnection, table: sa.Table) -> bool:
    """Checks if table exists in the current database."""

    try:
        await connection.execute(sa.select([table]).limit(1))
    except UndefinedTableError:
        return False
    except UndefinedColumnError:
        return True
    except Exception:
        return False
    else:
        return True


async def check_db_exists(connection: SAConnection, name: str) -> bool:
    """Checks if database with such name exists."""

    try:
        result = await connection.fetchrow(f"SELECT 1 FROM pg_database WHERE datname = '{name}';")
    except Exception as err:
        raise RuntimeError('ошибка при выполнении команды "_check_db_exists": %s' % err)
    return result is not None


async def create_db(connection: SAConnection, name: str):
    """CREATE DATABASE function with simple exception handling."""

    try:
        await connection.execute(f"CREATE DATABASE {name} WITH ENCODING 'UTF8';")
    except Exception as err:
        raise RuntimeError('ошибка при создании базы %s' % name) from err


async def create_table(connection: SAConnection, metadata: sa.MetaData, tables: List[sa.Table]):
    """CREATE TABLE function for specified tables."""

    async with connection.transaction():
        await connection.execute(
            dump_sql(metadata.create_all, bind=True)(tables=tables)
        )


async def check_table_health(connection: SAConnection, table: sa.Table) -> List[Exception]:
    """
    Checks table health data such as:
    - table exists
    - table is available for the current user
    - table has any data

    :returns: a list of errors and warnings if any
    """

    errors = []
    try:
        data = await connection.fetchrow(table.select().limit(1))
    except Exception as err:
        err = RuntimeError('ошибка при соединении с таблицей %s: %s' % (table.name, err))
        errors.append(err)
    else:
        if not data:
            err = Warning('нет данных в таблице %s' % table.name)
            errors.append(err)
    return errors


async def create_extensions(connection: SAConnection, extensions: List[str]):
    """Initialize postgresql extensions."""

    for ext in extensions:
        extension = await connection.fetchrow(f"""SELECT 1 FROM pg_extension WHERE extname='{ext}';""")
        if not extension:
            await connection.execute(f"""CREATE EXTENSION "{ext}";""")

