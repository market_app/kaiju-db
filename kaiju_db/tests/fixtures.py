import pytest
import sqlalchemy as sa

from kaiju_tools.tests.fixtures import *

from ..services import DatabaseService, UserFunction


DB_NAME = 'test_db'
DB_PORT = 5444

ROOT_CREDENTIALS = {
    'user': 'postgres',
    'password': 'postgres'
}

DEFAULT_CREDENTIALS = {
    'user': 'test',
    'password': 'test'
}


@pytest.fixture
def database_settings():
    """Default test database settings."""

    return {
        'settings': {
            'tablespace': '',
            'user': 'default',
        },
        'pool': {
            'host': 'localhost',
            'port': DB_PORT,
            'database': DB_NAME,
            'min_size': 4,
            'max_size': 64,
            'max_queries': 100000,
            'max_inactive_connection_lifetime': 30
        },
        'users': {
            'root': {
                'credentials': ROOT_CREDENTIALS,
                'role': 'rwx',
                'comment': 'рутовый юзер, которому доступны все действия,'
                           ' включая создание и удаление таблиц, данный юзер'
                           ' должен быть создан вручную'
            },
            'default': {
                'credentials': DEFAULT_CREDENTIALS,
                'role': 'rw',
                'comment': 'пользователь для обычных операций с базой во время работы'
                           ' приложения (создание, редактирование, чтение записей)'
            }
        },
        'extensions': ["uuid-ossp", "ltree"]
    }


@pytest.fixture
def database_service(logger, database_settings):
    """Returns a new instance of database service."""

    service = DatabaseService(
        app=None, metadata=None, logger=logger,
        **database_settings)
    return service


def _database_container(container_function):
    return container_function(
        image='postgres',
        name='postgres',
        version='12',
        ports={'5432': str(DB_PORT)},
        env={
            'POSTGRES_USER': 'postgres',
            'POSTGRES_PASSWORD': 'postgres'
        },
        sleep=3,
        healthcheck={
            'test': "pg_isready", 'interval': 100000000,
            'timeout': 3000000000, 'start_period': 1000000000, 'retries': 3
        }
    )


@pytest.fixture
def database(container):
    """
    Returns a new database container. See `kaiju_tools.tests.fixtures.container`
    for more info.
    """

    return _database_container(container)


@pytest.fixture(scope='session')
def per_session_database(per_session_container):
    """
    Returns a new database container. See `kaiju_tools.tests.fixtures.per_session_container`
    for more info.
    """

    return _database_container(per_session_container)


@pytest.fixture
def test_table():
    t = sa.Table(
        'test_table', sa.MetaData(),
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('value', sa.Boolean, default=False),
        sa.Column('other_value', sa.Boolean, default=False),
        sa.Column('secret_value', sa.TEXT, default=''),
        sa.Column('t', sa.TIMESTAMP)
    )
    return t


@pytest.fixture
def test_composite_table():
    t = sa.Table(
        'test_composite_table', sa.MetaData(),
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('key', sa.Integer, primary_key=True),
        sa.Column('value', sa.Boolean, default=False),
        sa.Column('other_value', sa.Boolean, default=False),
        sa.Column('secret_value', sa.TEXT, default=''),
        sa.Column('t', sa.TIMESTAMP)
    )
    return t


@pytest.fixture
def test_function():

    class MyFunction(UserFunction):
        package = 'utils'
        name = 'my_func'
        type = sa.types.Integer
        body = """
        FUNCTION my_func(v integer) RETURNS integer AS $$
        BEGIN
        RETURN v * v;
        END; $$
        LANGUAGE PLPGSQL
        """

        def __init__(self, v: int):
            super(MyFunction, self).__init__(v)

    return MyFunction

