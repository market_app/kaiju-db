"""
This module contains some services for database management.

:class:`.DatabaseService` is a database connector able to proxy sql requests to
the asyncpg driver. It also inits the whole database from config upon an app's start.

:class:`.SQLService` is a base class for your db data-related RPC compatible
services. It provides some useful methods for data manipulation ready to use
in your RPC server, like get, create, update etc.

Classes
-------

"""

from __future__ import annotations

import sqlalchemy.sql.functions

import abc
import asyncio
from typing import List, AsyncGenerator, Union, Optional

try:
    from typing import TypedDict
except ImportError:
    # python 3.6 compatibility
    from typing_extensions import TypedDict

import sqlalchemy as sa
from sqlalchemy.event import listen
import asyncpgsa
from asyncpgsa.connection import _dialect, SAConnection
from asyncpg import ForeignKeyViolationError, UniqueViolationError

from kaiju_tools.class_registry import AbstractClassRegistry
from kaiju_tools.serialization import dumps, loads
from kaiju_tools.exceptions import ValidationError, NotFound, Conflict, InternalError, MethodNotAllowed
from kaiju_tools.services import Service, ContextableService
from kaiju_tools.rpc.abc import ServerSessionFlag, Session

from .roles import *
from .functions import *

__all__ = (
    'UserFunction', 'FunctionsRegistry', 'functions_registry',
    'DatabaseServiceCompatibleApp',
    'DatabaseService', 'SQLService', 'ErrorCodes', 'DDL'
)


class ErrorCodes:
    """These error codes should be commonly used for SQL related errors."""

    EXISTS = 'query.exists'
    REFERENCE_NOT_FOUND = 'query.reference_not_found'
    NOT_FOUND = 'query.not_found'
    INTERNAL_ERROR = 'query.internal_error'
    FIELD_DOES_NOT_EXISTS = 'query.field_does_not_exists'
    INVALID_CONDITION = 'query.invalid_condition_command'
    INVALID_ORDERING = 'query.invalid_ordering_command'
    INVALID_PAGINATION_OFFSET = 'query.invalid_pagination_offset'
    INVALID_PAGINATION_LIMIT = 'query.invalid_pagination_limit'
    INVALID_COLUMN = 'query.invalid_insert_column'


def DDL(target, identifier, body):
    """
    Specifies literal SQL DDL to be executed by the database.  DDL objects
    function as DDL event listeners, and can be subscribed to those events
    listed in :class:`.DDLEvents`, using either :class:`.Table` or
    :class:`.MetaData` objects as targets.   Basic templating support allows
    a single DDL instance to handle repetitive tasks for multiple tables.

    example create function before create table task:

    .. code-block:: python

        DDL(
            task,
            "before_create",
            \"""
                CREATE OR REPLACE FUNCTION TaskSortingIncrement()
                RETURNS trigger AS $BODY$BEGIN
                   NEW.sort:= (SELECT COALESCE(max(sort), 0) FROM task WHERE status_id = NEW.status_id) + 1;
                   RETURN NEW;
                END
                $BODY$
                LANGUAGE plpgsql VOLATILE
                COST 100;
            \"""
        )

    example create trigger after create table task:

    .. code-block:: python

        DDL(
            task,
            "after_create",
            \"""
                DROP TRIGGER IF EXISTS TaskTriggerBeforeInsert
                ON task;
                CREATE TRIGGER TaskTriggerBeforeInsert
                BEFORE INSERT
                ON task
                FOR EACH ROW
                EXECUTE PROCEDURE TaskSortingIncrement();
            \"""
        )

    :param target: - table
    :param identifier: - see doc https://docs.sqlalchemy.org/en/13/core/event.html
    :param body: - sql
    """

    func = sa.DDL(body)
    listen(
        target,
        identifier,
        func.execute_if(dialect='postgresql')
    )


class UserFunction(sa.sql.functions.GenericFunction):
    """
    Define your function interface and executable sql body.

    .. code-block:: python

        class MyFunction(UserFunction):
            package = 'utils'
            name = 'my_func'
            type = sa.types.Integer
            body = "my_func(v integer) RETURNS integer AS $$ BEGIN RETURN v; END; $$ LANGUAGE PLPGSQL;"

            def __init__(self, v: int):
                return super().__init__(v)

    Now you can use this function from python. See `https://docs.sqlalchemy.org/en/13/core/functions.html`
    for detail about generic functions methods.

    .. code-block:: python

        await self.app.db.fetchval(MyFunction(42).select())

    To make your function actually available in your app you need to tell
    the database service about it, i.e. register it in the function registry

    .. code-block::

        from kaiju_db.services import functions_registry

        functions_registry.register_class(UserFunction)

    or (if you have many)

    .. code-block::

        from kaiju_db.services import functions_registry

        import my_functions

        functions_registry.register_from_module(my_functions)

    """

    body: str  #: executable sql string

    @classmethod
    def sql(cls) -> str:
        body = cls.body.strip()
        if not body.lower().startswith('function'):
            body = f'FUNCTION {body}'
        if not body.endswith(';'):
            body += ';'
        return body


class FunctionsRegistry(AbstractClassRegistry):
    """A simple registry for SQL functions."""

    base_classes = (UserFunction,)

    @staticmethod
    def class_key(obj) -> str:
        return obj.name


functions_registry = FunctionsRegistry(raise_if_exists=False)


class DatabaseServiceCompatibleApp(abc.ABC):
    """DatabaseService compatible interface (hint) for a web application."""

    db_meta: sa.MetaData  #: a metadata object with all tables should be stored here
    functions_registry: FunctionsRegistry  #: optional registry for user SQL functions


class DatabaseService(ContextableService):
    """
    DatabaseService is a service which holds a database pool and table metadata
    as well at is handles database initialization from db settings dictionary
    (see `kaiju_db.settings` for more info).

    It can be used directly in the context:

    .. code-block:: python

        async with DatabaseService(app=None, logger=logger, **database_settings) as db:
            await db.execute('SELECT 1;')


    Or as a part of the :class:`kaiju_tools.services.ServiceContextManager` if you
    pass its init settings.

    .. code-block:: python

        db = {
            'cls': 'DatabaseService',
            'name': 'db',
            'info': 'Database connector.',
            'settings': DB_SETTINGS
        }

        services = ServiceContextManager(app, settings=[db])
        services.db  # the database service


    After initialization you can address it as a pool because it proxies all
    the pool's attributes:

    .. code-block:: python

        async with db.acquire() as conn:
            async with conn.transaction():
                await conn.insert(sql)


    :param app: web app instance
    :param metadata: metadata object
    :param logger: optional logger instance
    :param init_db: init db on start from a root user
    :param functions: dict or registry of SQL function classes
    :param settings: database settings (see `kaiju_db.settings`)
    """

    service_name = 'db'
    ROOT_DB_NAME = 'postgres'

    def __init__(
            self, app: DatabaseServiceCompatibleApp = None,
            metadata: sa.MetaData = None,
            functions: Union[FunctionsRegistry, dict] = None,
            logger=None, init_db=True, **settings):
        super().__init__(app=app, logger=logger)
        if metadata:
            self._metadata = metadata
        elif app:
            self._metadata = app.db_meta
        else:
            self._metadata = sa.MetaData()
        if functions:
            self._functions_registry = functions
        elif hasattr(app, 'function_registry'):
            self._functions_registry = app.function_registry
        else:
            self._functions_registry = functions_registry
        self._init_db = init_db
        self._settings = settings
        self._pool = None

    def __getattr__(self, item):
        if self._pool is not None:
            return getattr(self._pool, item)
        else:
            return super().__getattribute__(item)

    async def init(self):
        if self._init_db:
            await self._create_db_if_not_exists()
            await self._create_users_and_tables()
        user = self._settings["settings"]["user"]
        self._pool = await self._init_pool(user)

    async def close(self):
        await self._pool.close()

    @property
    def closed(self) -> bool:
        return self._pool is None

    @property
    def metadata(self) -> sa.MetaData:
        return self._metadata

    @property
    def db_name(self) -> str:
        return self._settings['pool']['database']

    def add_table(self, table: sa.Table) -> sa.Table:
        """Adds a table to service's metadata."""

        if table.name in self._metadata:
            return self._metadata.tables[table.name]
        else:
            self._metadata._add_table(table.name, None, table)
            table.metadata = self._metadata
            return table

    async def _init_pool(self, user: str, db_name: str = None):

        async def _init_connection(connection: SAConnection):
            await connection.set_type_codec('jsonb', encoder=dumps, decoder=loads, schema='pg_catalog')

        def _encoder(value):
            return value

        _dialect._json_serializer = _encoder
        _dialect._json_deserializer = loads
        try:
            if db_name:
                settings = {**self._settings['pool']}
                settings['database'] = db_name
            else:
                settings = self._settings['pool']
                db_name = self.db_name
            credentials = self._settings['users'][user]['credentials']
            self.logger.debug('Initializing a new pool for user "%s" to "%s"', user, db_name)
            new_pool = await asyncpgsa.create_pool(
                **credentials,
                **settings,
                init=_init_connection,
                dialect=_dialect,
            )
        except Exception as err:
            self.logger.error('Error on pool "%s" init for user "%s": "%s"', db_name, user, err)
            raise
        else:
            return new_pool

    async def _init_root_pool(self, db_name=ROOT_DB_NAME):
        return await self._init_pool(user='root', db_name=db_name)

    async def _create_db_if_not_exists(self):
        pool = await self._init_root_pool()
        async with pool as root_pool:
            name = self._settings["pool"]['database']
            async with root_pool.acquire() as conn:
                self.logger.debug('Checking db "%s"', name)
                if not await check_db_exists(conn, name):
                    self.logger.debug('Creating db "%s"', name)
                    await create_db(conn, name)
                else:
                    self.logger.debug('DB "%s" seems OK', name)

    async def _create_users_and_tables(self):

        async def _create_users(connection: SAConnection):
            db_users = self._settings["users"]
            for user, user_settings in db_users.items():
                if user != 'root':
                    self.logger.debug('Checking user "%s"', user)
                    credentials = user_settings['credentials']
                    try:
                        if not await check_role_exists(connection, credentials['user']):
                            self.logger.info('Creating user "%s"', user)
                            await create_user(
                                connection,
                                self._settings['pool']['database'],
                                credentials['user'],
                                credentials['password'],
                                role=user_settings['role'])
                        else:
                            self.logger.debug('User "%s" seems OK', user)
                    except Exception as err:
                        raise RuntimeError(
                            'Error adding user "%s": "%s"'
                            % (user, err)) from err

        async def _create_extensions(connection: SAConnection):
            extensions = self._settings.get("extensions")
            self.logger.debug('Creating extensions: "%s"', extensions)
            await create_extensions(connection, extensions)

        async def _init_db(connection: SAConnection):
            not_created_tables = []

            for table in self._metadata.sorted_tables:
                self.logger.debug('Checking table "%s"', table.name)
                try:
                    if not await check_table_exists(connection, table):
                        not_created_tables.append(table)
                        continue
                    else:
                        self.logger.debug('Table "%s" seems OK', table.name)
                    for exc in await check_table_health(connection, table):
                        self.logger.error(exc)
                except Exception as err:
                    self.logger.error('Error initializing table "%s": "%s"', table.name, err)
                    raise err

            if not not_created_tables:
                return

            for table in not_created_tables:
                self.logger.info('Creating table "%s"', table)
                await connection.execute(dump_sql(self._metadata.create_all, bind=True)(tables=[table]))
                for exc in await check_table_health(connection, table):
                    self.logger.error(exc)

        async def _create_functions(conn):
            for _function_key in self._functions_registry:
                _function = self._functions_registry[_function_key]
                if issubclass(_function, UserFunction):
                    _function = _function.sql()
                sql = f"CREATE OR REPLACE {_function}"
                await conn.execute(sql)

        async with await self._init_root_pool(db_name=self.db_name) as root_pool:
            async with root_pool.acquire() as conn:
                await _create_extensions(conn)
                await _create_functions(conn)
                await _init_db(conn)
            async with root_pool.acquire() as conn:
                await _create_users(conn)


class SQLService(Service, abc.ABC):
    """
    Just a base SQL service interface with common commands and errors.
    Optimized for a single primary key only with a name "id"

    Example of use:

    You need to set your own table and, if needed, define column whitelists
    for different operations.

    .. code-block:: python

        class MyService(SQLService):
            table = my_table
            select_columns = {'id', 'data', 'flag'}
            insert_columns = {'data', 'flag'}
            update_columns = {'flag'}

    Here you can access some basic methods: "exists", "get", "create", "update",
    "delete" and their bulk versions (for multiple objects at once).

    Also there is a simply querying interface named "list"

    You can change any of SQL base for these commands by redefining query
    constructors.

    .. code-block:: python

        class MyService(SQLService):

            ...

            def _create_get_query(id, columns, table=None):
                ... # custom query

            def _create_m_get_query(self, id: list, columns, table=None):
                ... # custom query

    :param app: web app instance
    :param database_service: database service instance or instance name
    :param logger: optional logger instance
    """

    class _H_List(TypedDict):
        """Type hinting for a list query."""

        count: Optional[int]          #: total rows matching the query, None if count hasn't been requested
        offset: int                   #: row offset for this selection
        page: Optional[int]           #: current page number, None if count hasn't been requested
        pages: Optional[int]          #: total pages, None if count hasn't been requested
        on_page: int                  #: number of rows on this page
        data: Optional[List[dict]]    #: returned rows, None if limit was set to 0

    ErrorCodes = ErrorCodes
    database_service_class = DatabaseService
    DEFAULT_ROW_LIMIT = 24  #: defaults of LIMIT on queries
    MAX_ROW_LIMIT = 1000    #L max size of queries

    select_columns = None  #: you can specify a whitelist of output columns here
    select_columns_blacklist = None
    update_columns = None  #: you may specify columns for update here
    update_columns_blacklist = None
    insert_columns = None  #: you may specify insert columns here
    insert_columns_blacklist = None
    table = None            #: here should be your table

    virtual_columns = None  # virtual SQL columns (i.e. functions) names and definitions
                            # virtual columns can be selected but never updated / inserted

    def __init__(self, app, database_service: Union[database_service_class, str], logger=None):

        def _prepare_whitelist(whitelist, blacklist):
            if blacklist:
                whitelist = set(col.name for col in self.table.columns) if whitelist is None else set(whitelist)
                return frozenset(whitelist.difference(set(blacklist)))
            elif whitelist:
                return frozenset(whitelist)

        super().__init__(app=app, logger=logger)

        self._db = self.discover_service(database_service, cls=self.database_service_class)
        self.table = self._db.add_table(self.table)  # registers table in the db service meta

        self._primary_keys = frozenset((col.name for col in self.table.primary_key))
        self._composite_primary_key = len(self._primary_keys) > 1
        if self._composite_primary_key:
            self._primary_key_condition = self._create_primary_key_condition_for_composite_key
            self._list_primary_key_condition = self._create_list_primary_key_condition_for_composite_key
            self._primary_key = self._primary_key_list = list(self.table.primary_key)
        elif len(self._primary_keys) == 1:
            self._primary_key_condition = self._create_primary_key_condition_for_single_key
            self._list_primary_key_condition = self._create_list_primary_key_condition_for_single_key
            self._primary_key = next(iter(self.table.primary_key))
            self._primary_key_list = [self._primary_key]
        else:
            self._primary_key_condition = self._raise_primary_key_condition_for_no_keys
            self._list_primary_key_condition = self._raise_primary_key_condition_for_no_keys
            self._primary_key = None

        self.select_columns = _prepare_whitelist(self.select_columns, self.select_columns_blacklist)
        self.insert_columns = _prepare_whitelist(self.insert_columns, self.insert_columns_blacklist)
        self.update_columns = _prepare_whitelist(self.update_columns, self.update_columns_blacklist)

    @property
    def routes(self) -> dict:
        """Can be used as default routes in RPC services."""

        return {

            'exists': self.exists,
            'get': self.get,
            'create': self.create,
            'update': self.update,
            'delete': self.delete,

            'm_exists': self.m_exists,
            'm_get': self.m_get,
            'm_create': self.m_create,
            'm_update': self.m_update,
            'm_delete': self.m_delete,

            'list': self.list
        }

    @staticmethod
    def _get_condition_hook(sql, session: Union[Session, ServerSessionFlag]):
        """
        You can use this method to add a custom post-conditional SQL hook
        for GET, LIST or EXISTS commands. Use `AbstractRPCCompatible` session
        management methods to extract user info.
        """

        return sql

    @staticmethod
    def _insert_condition_hook(sql, session: Union[Session, ServerSessionFlag]):
        """
        You can use this method to add a custom post-conditional SQL hook
        for CREATE commands. Use `AbstractRPCCompatible` session
        management methods to extract user info.
        """

        return sql

    @staticmethod
    def _update_condition_hook(sql, session: Union[Session, ServerSessionFlag]):
        """
        You can use this method to add a custom post-conditional SQL hook
        for UPDATE commands. Use `AbstractRPCCompatible` session
        management methods to extract user info.
        """

        return sql

    @staticmethod
    def _delete_condition_hook(sql, session: Union[Session, ServerSessionFlag]):
        """
        You can use this method to add a custom post-conditional SQL hook
        for DELETE commands. Use `AbstractRPCCompatible` session
        management methods to extract user info.
        """

        return sql

    def _create_primary_key_condition_for_single_key(self, sql, _id):
        return sql.where(self._primary_key == _id)

    def _create_list_primary_key_condition_for_single_key(self, sql, _id):
        if not isinstance(_id, (list, tuple, set)):
            _id = [_id]
        return sql.where(self._primary_key.in_(_id))

    def _parse_composite_id(self, _id):

        if not isinstance(_id, dict):
            raise ValidationError(
                message='Tables with composite primary keys'
                        ' only support explicit id mappings with field names.'
                        ' Expected an object in "id".',
                data={'id': '_id'})

        conditions, col = [], None

        try:
            for col in self._primary_key:
                conditions.append(col == _id[col.name])
        except KeyError:
            raise ValidationError(
                message='Primary key field is not present in the primary key.',
                data={'id': _id, 'field': col.name})
        return sa.and_(*conditions)

    def _create_list_primary_key_condition_for_composite_key(self, sql, _id):
        if not isinstance(_id, (list, tuple, set)):
            _id = [_id]
        condition = sa.or_(*(self._parse_composite_id(n) for n in _id))
        return sql.where(condition)

    def _create_primary_key_condition_for_composite_key(self, sql, _id: dict):
        condition = self._parse_composite_id(_id)
        return sql.where(condition)

    def _raise_primary_key_condition_for_no_keys(self, *_, **__):
        raise MethodNotAllowed(
            message='Direct referencing objects in the table is forbidden'
                    ' because there are no primary keys in this table.')

    async def _wrap_get(self, func):
        """Wraps a GET query to reraise SQL errors in a different format."""

        try:
            return await func
        except Exception as exc:
            raise InternalError(
                'Error processing query.',
                base_exc=exc, service=self.service_name)

    async def _wrap_insert(self, func):
        """Wraps an INSERT query to reraise SQL errors in a different format."""

        try:
            return await func
        except UniqueViolationError as exc:
            raise Conflict(
                'Name conflict. Object already exists.',
                code=self.ErrorCodes.EXISTS,
                base_exc=exc, service=self.service_name)
        except ForeignKeyViolationError as exc:
            raise NotFound(
                'An object referenced by this object doesn\'t exist.',
                code=self.ErrorCodes.REFERENCE_NOT_FOUND,
                base_exc=exc, service=self.service_name)
        except Exception as exc:
            raise InternalError(
                'Error processing query.',
                code=self.ErrorCodes.INTERNAL_ERROR,
                base_exc=exc, service=self.service_name)

    def _wrap_update(self, func):
        """Wraps an UPDATE query to reraise SQL errors in a different format."""

        return self._wrap_insert(func)

    def _wrap_delete(self, func):
        """Wraps a DELETE query to reraise SQL errors in a different format."""

        return self._wrap_get(func)

    def _create_exists_query(self, _id, session=None, table=None):
        if table is None:
            table = self.table
        sql = table.select().with_only_columns([sa.text('1')])
        sql = self._primary_key_condition(sql, _id)
        sql = self._get_condition_hook(sql, session)
        sql = sql.limit(1)
        return sql

    async def exists(self, id, session=ServerSessionFlag) -> bool:
        """
        Returns True if object exists. False otherwise.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.exists({'id': 1, 'key': 'abc'})

        """

        sql = self._create_exists_query(id, session=session)
        result = await self._wrap_get(self._db.fetchrow(sql))
        return bool(result)

    def _create_m_exists_query(self, _id: list, session=None, table=None):
        if table is None:
            table = self.table
        sql = table.select().with_only_columns(self._primary_key_list)
        sql = self._list_primary_key_condition(sql, _id)
        sql = self._get_condition_hook(sql, session)
        return sql

    async def m_exists(self, id: list, session=ServerSessionFlag):
        """
        Returns a set of existing IDs for a list of IDs.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.m_exists([{'id': 1, 'key': 'abc'}, ...])

        """

        sql = self._create_m_exists_query(id, session=session)
        rows = await self._wrap_get(self._db.fetch(sql))
        if rows:
            if self._composite_primary_key:
                result = list(rows)
                return result
            else:
                key = next(next(iter(rows)).keys())  # extracting back the primary key
                result = frozenset(row[key] for row in rows)
                return result
        else:
            return frozenset()

    def _sql_get_column(self, table, column: str):
        """Returns an SQLAlchemy column by its name."""

        try:
            return self.table.columns[column]
        except KeyError:
            raise ValidationError(
                'Requested field doesn\'t exists',
                obj=table.name, field=column, service=self.service_name,
                code=self.ErrorCodes.FIELD_DOES_NOT_EXISTS)

    def _sql_get_columns(self, columns, table=None) -> list:
        """
        Constructs a list of columns from their names. By default it uses
        `self.table` as table.

        .. code-block:: python

            '*'             # all columns
            "..."           # single column
            ["...", "..."]  # multiple columns

        """

        if table is None:
            table = self.table

        if not columns:
            return []

        elif columns == '*':
            if self.select_columns is None:
                columns = self.table.columns
            else:
                columns = [col for col in self.table.columns if col.name in self.select_columns]
            if self.virtual_columns:
                virtual = [
                    sa.text(value + f' AS {name}')
                    for name, value in self.virtual_columns.items()
                ]
                columns = [*columns, *virtual]
        else:
            if isinstance(columns, str):
                columns = [columns]

            if self.virtual_columns:

                _columns = []

                for column in columns:
                    if column in self.virtual_columns:
                        value = self.virtual_columns[column]
                        _columns.append(sa.text(value + f' AS {column}'))
                    elif self.select_columns:
                        if column in self.select_columns:
                            _columns.append(self._sql_get_column(table, column))
                    else:
                        _columns.append(self._sql_get_column(table, column))

                columns = _columns

            else:
                if self.select_columns:
                    columns = [
                        self._sql_get_column(table, key) for key in columns
                        if key in self.select_columns
                    ]
                else:
                    columns = [
                        self._sql_get_column(table, key) for key in columns
                    ]

        return columns

    @staticmethod
    def _filter_columns(columns: List[sa.Column], whitelist: Optional[frozenset]):
        if whitelist is None:
            return columns
        else:
            return [col for col in columns if col.name in whitelist]

    def _create_get_query(self, _id, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        sql = table.select().with_only_columns(columns)
        sql = self._primary_key_condition(sql, _id)
        sql = self._get_condition_hook(sql, session)
        sql = sql.limit(1)
        return sql

    async def get(self, id, columns='*', session=ServerSessionFlag):
        """Returns information about an object.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.get({'id': 1, 'key': 'abc'}, ...)

        :raises NotFound:
        """

        if columns is None:
            raise ValidationError('Null "columns" param is not allowed in a get query.')

        sql = self._create_get_query(id, columns, session=session)
        result = await self._wrap_get(self._db.fetchrow(sql))

        if not result:
            raise NotFound(
                'Object doesn\'t exist.',
                service=self.service_name,
                object_id=str(id), code=self.ErrorCodes.NOT_FOUND)

        if columns:
            return result

    def _create_m_get_query(self, _id: list, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        sql = table.select().with_only_columns(columns)
        sql = self._list_primary_key_condition(sql, _id)
        sql = self._get_condition_hook(sql, session)
        return sql

    async def m_get(self, id: list, columns='*', session=ServerSessionFlag):
        """Returns multiple object data. Objects that don't exist will be skipped.
        Returns all data at once without pagination. Use `SQLService.list` if
        you want pagination or sorting.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.m_get([{'id': 1, 'key': 'abc'}, ...], ...)

        """

        sql = self._create_m_get_query(id, columns, session=session)
        result = await self._wrap_get(self._db.fetch(sql))
        return result

    def _create_delete_query(self, _id, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        sql = table.delete()
        sql = self._primary_key_condition(sql, _id)
        sql = self._delete_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def delete(self, id, columns=None, session=ServerSessionFlag):
        """
        Removes a single object from a table.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.delete({'id': 1, 'key': 'abc'}, ...)

        :raises NotFound: if object doesn't exist or already was deleted
        """

        sql = self._create_delete_query(id, columns, session=session)

        if columns:
            result = await self._wrap_delete(self._db.fetchrow(sql))
        else:
            result = await self._wrap_delete(self._db.execute(sql))
            if result != 'DELETE 1':
                result = None

        if not result:
            raise NotFound(
                'Object doesn\'t exist thus it can\'t be removed.',
                service=self.service_name,
                object_id=str(id), code=self.ErrorCodes.NOT_FOUND)

        if columns:
            return result

    def _create_m_delete_query(self, _id, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        sql = table.delete()
        sql = self._list_primary_key_condition(sql, _id)
        sql = self._delete_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def m_delete(self, id: list, columns=None, session=ServerSessionFlag):
        """
        Removes multiple objects from a table. Non-existing objects will be skipped.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.m_delete([{'id': 1, 'key': 'abc'}, ...], ...)

        """

        sql = self._create_m_delete_query(id, columns, session=session)
        result = await self._wrap_delete(self._db.fetch(sql))
        return result

    @staticmethod
    def prepare_insert_data(session, data: dict):
        """You may define your custom row init logic here."""

        return data

    def _validate_row(self, data: dict, whitelist: frozenset):
        """Validates and prepares insert row data."""

        if not data:
            raise ValidationError(
                'There are no columns for insert.',
                data=data,
                service=self.service_name,
                allowed_columns=self.insert_columns,
                code=self.ErrorCodes.INVALID_COLUMN)

        if whitelist:
            if not set(data).issubset(whitelist):
                raise ValidationError(
                    'There are not consumed columns in the update statement.',
                    data=data,
                    service=self.service_name,
                    allowed_columns=self.insert_columns,
                    code=self.ErrorCodes.INVALID_COLUMN)

    def _create_insert_query(self, data, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        self._validate_row(data, whitelist=self.insert_columns)
        data = self.prepare_insert_data(session, data)
        sql = table.insert().values(data)
        sql = self._insert_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def create(self, data: dict, columns='*', session=ServerSessionFlag):
        """Creates a single object."""

        sql = self._create_insert_query(data, columns, session=session)
        if columns:
            result = await self._wrap_insert(self._db.fetchrow(sql))
            return result
        else:
            await self._wrap_insert(self._db.execute(sql))

    def _create_m_insert_query(self, data: List[dict], columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        _data = []
        for row in data:
            self._validate_row(row, whitelist=self.insert_columns)
            _data.append(self.prepare_insert_data(session, row))
        sql = table.insert().values(data)
        sql = self._insert_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def m_create(self, data: List[dict], columns='*', session=ServerSessionFlag):
        """Creates a single object."""

        sql = self._create_m_insert_query(data, columns, session=session)
        if columns:
            result = await self._wrap_insert(self._db.fetch(sql))
            return result
        else:
            await self._wrap_insert(self._db.execute(sql))

    @staticmethod
    def prepare_update_data(session, data: dict):
        """You may define your custom row update logic here."""

        return data

    def _create_update_query(self, _id, data: dict, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        self._validate_row(data, whitelist=self.update_columns)
        data = self.prepare_update_data(session, data)
        sql = table.update().values(data)
        sql = self._primary_key_condition(sql, _id)
        sql = self._update_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def update(self, id, data: dict, columns='*', session=ServerSessionFlag) -> dict:
        """
        Updates a single object. Raises error if object doesn't exist.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.update({'id': 1, 'key': 'abc'}, ...)

        """

        sql = self._create_update_query(id, data, columns, session=session)

        if columns:
            result = await self._wrap_update(self._db.fetchrow(sql))
        else:
            result = await self._wrap_update(self._db.execute(sql))
            if result != 'UPDATE 1':
                result = None

        if not result:
            raise NotFound(
                'Object doesn\'t exist thus it can\'t be updated.',
                service=self.service_name,
                object_id=str(id), code=self.ErrorCodes.NOT_FOUND)

        if columns:
            return result

    def _create_m_update_query(self, _id: list, data: dict, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        self._validate_row(data, whitelist=self.update_columns)
        data = self.prepare_update_data(session, data)
        sql = table.update().values(data)
        sql = self._list_primary_key_condition(sql, _id)
        sql = self._update_condition_hook(sql, session)
        sql = sql.returning(*columns)
        return sql

    async def m_update(self, id: list, data: dict, columns='*', session=ServerSessionFlag):
        """
        Updates multiple objects with the same data. Non-existing objects will be skipped.

        If you have composite primary keys you should pass dictionary objects
        in id field, like this:

        .. code-block:: python

            await service.m_update([{'id': 1, 'key': 'abc'}, ...], ...)

        """

        sql = self._create_m_update_query(id, data, columns, session=session)
        if columns:
            result = await self._wrap_update(self._db.fetch(sql))
            return result
        else:
            await self._wrap_update(self._db.execute(sql))

    def _create_conditions(self, sql, conditions, table=None):
        """
        Adds SQL conditions to an sql query.
        Allows some simple queries to be constructed.

        .. code-block:: python

            [ ... ]  # OR
            { ... }  # AND

            { "...": "..." }            # ==
            { "...": ["...", "..."] }   # IN

        """

        if table is None:
            table = self.table

        def _get_conditions(condition):
            if type(condition) is dict:
                _condition = []
                for key, value in condition.items():
                    if self.select_columns and key not in self.select_columns:
                        raise ValidationError(
                            'Invalid condition requested. Unknown condition key.',
                            service=self.service_name,
                            key=key,
                            code=self.ErrorCodes.INVALID_CONDITION)

                    column = self._sql_get_column(table, key)
                    if type(value) is list:
                        _c = column.in_(value)
                    elif type(value) is dict:
                        _sub_cond = []
                        for key, _value in value.items():
                            key = key.lower()
                            if key == 'gt':
                                _sub_cond.append(column > _value)
                            elif key == 'lt':
                                _sub_cond.append(column < _value)
                            elif key == 'ge':
                                _sub_cond.append(column >= _value)
                            elif key == 'le':
                                _sub_cond.append(column <= _value)
                            elif key == 'eq':
                                _sub_cond.append(column == _value)
                            elif key == 'like':
                                _sub_cond.append(column.like(_value))
                            elif key == '~':
                                _sub_cond.append(sa.func.lower(column).op('~')(sa.func.lower(_value)))
                            else:
                                raise ValidationError(
                                    'Invalid condition requested.',
                                    service=self.service_name,
                                    condition_key=key,
                                    code=self.ErrorCodes.INVALID_CONDITION)
                        _c = sa.and_(*_sub_cond)
                    else:
                        _c = column == value
                    _condition.append(_c)
                return sa.and_(*_condition)
            elif type(condition) is list:
                _condition = [_get_conditions(c) for c in condition]
                return sa.or_(*_condition)
            else:
                raise ValidationError(
                    'Invalid condition requested.'
                    ' Condition must be an object or an array of objects.',
                    service=self.service_name,
                    code=self.ErrorCodes.INVALID_CONDITION)

        conditions = _get_conditions(conditions)
        return sql.where(conditions)

    def _create_sort(self, sql, ordering, table=None):
        """
        Adds SQL ordering command to a query.

        .. code-block:: python

            "..."            # sort ASC by a key
            {"desc": "..."}  # sort DESC by a key
            [ ..., ... ]     # sort by multiple keys in order

        """

        def _get_ordering(o):
            if isinstance(o, dict):
                key, name = next(iter(o.items()), None)
                key = key.lower()
            else:
                key = 'asc'
                name = o
            if type(name) is str:
                column = self._sql_get_column(table, name)
            else:
                column = name
            if key == 'desc':
                column = sa.desc(column)
            elif key == 'asc':
                pass
            else:
                raise ValidationError(
                    'Invalid ordering requested.',
                    service=self.service_name,
                    ordering_key=key,
                    code=self.ErrorCodes.INVALID_ORDERING)
            return sa.nullslast(column)

        if table is None:
            table = self.table
        if type(ordering) is list:
            sql = sql.order_by(*(_get_ordering(o) for o in ordering))
        else:
            sql = sql.order_by(_get_ordering(ordering))
        return sql

    def _create_pagination(self, sql, offset, limit):
        """
        Adds pagination directives to an SQL query.
        Limit < 1 transforms to 1 and offset < 0 transforms to 0 by default.
        """

        if offset:
            try:
                offset = max(0, int(offset))
            except ValueError:
                raise ValidationError(
                    'Invalid offset.',
                    service=self.service_name,
                    pagination_key='offset', value=offset,
                    code=self.ErrorCodes.INVALID_PAGINATION_OFFSET)
            sql = sql.offset(offset)

        if limit is None:
            limit = self.DEFAULT_ROW_LIMIT

        try:
            limit = min(max(1, int(limit)), self.MAX_ROW_LIMIT)
        except ValueError:
            raise ValidationError(
                'Invalid limit.',
                service=self.service_name,
                pagination_key='limit', value=limit,
                code=self.ErrorCodes.INVALID_PAGINATION_LIMIT)

        sql = sql.limit(limit)
        return sql

    def _create_count_query(self, conditions, precision=None, session=None, table=None):
        """Uses COUNT(id) for slow precise count or TABLESAMPLE for estimate."""

        if table is None:
            table = self.table
        key = next(iter(self._primary_key_list))
        sql = table.select().with_only_columns([sa.func.count(key)])
        if conditions:
            sql = self._create_conditions(sql, conditions, table=table)
        sql = self._get_condition_hook(sql, session)
        if precision:
            precision = min(max(1, precision), 100)
            sql = f'SELECT count({key.name})' \
                  f' FROM {table.name} TABLESAMPLE system({precision});'
        return sql

    def _create_list_query(self, conditions, sort, offset, limit, columns, session=None, table=None):
        if table is None:
            table = self.table
        columns = self._sql_get_columns(columns, table=table)
        sql = table.select().with_only_columns(columns)
        if conditions:
            sql = self._create_conditions(sql, conditions, table=table)
        sql = self._get_condition_hook(sql, session)
        if sort:
            sql = self._create_sort(sql, sort, table=table)
        if offset or limit:
            sql = self._create_pagination(sql, offset, limit)
        return sql

    @staticmethod
    def _get_page_count(count: int, offset: int, page_size: int) -> (int, int):
        """Supports arbitrary offsets. Returns current page number and number of pages."""

        if page_size:
            n = count - offset
            pages = max(1, count // page_size + bool(n % page_size))
            page = max(1, offset // page_size + bool(n % page_size))
            return page, pages
        else:
            return None, None

    async def list(
            self, conditions=None, sort=None, offset=0, limit=DEFAULT_ROW_LIMIT,
            count=True, precision=None, columns='*', session=ServerSessionFlag) -> _H_List:
        """
        Lists rows with pagination and conditions.

        Condition example:

        .. code-block:: python

            service.list(
                conditions={
                    'tag': ['tag_1', 'tag_2', 'tag_3']  # IN condition
                    'active': True                      # EQ condition
                    'value': {'gt': 41, 'le': 42'}      # num conditions,
                    'text': {'like': 'sht'}             # text field "likeness"
                }
            )

        Available numeric conditions: gt, lt, ge, le, eq
        Available other conditions: like

        Sort example:

        .. code-block:: python

            service.list(
                sort=['tag', {'desc': 'timestamp'}]     # order matters
            )

        Available sorting conditions: desc, asc (default)

        You can use this method for counting without returning any results.
        Just set the limit to zero. Optionally, you can also set the counting
        precision.

        .. code-block::

            service.list(
                conditions={ ... },
                precision=50
                limit=0
            )

        Contrary, if you don't need counting, you can disable it. No count / page
        data will be available then.

        .. code-block::

            service.list(
                conditions={ ... },
                count=False
            )

        Precision uses a number of table samples to estimate the count. If
        the precision is set to 0 or None, then the exact count will be performed.

        .. attention::

            Precision is not working at the moment (don't know why).

        If count argument is False, then count, page and pages result values
        will be None.

        If columns is None, then data will be None and on_page will be zero.

        :param conditions: optional query conditions
        :param sort: optional row ordering
        :param offset: optional row offset
        :param limit: optional row limit
        :param count: set to False to disable page counting
        :param precision: count precision, if None then table sampling won't be used
            must be from 0 to 100
        :param columns: columns to return
        :returns: dict
            This method may return different data depending on the provided params

        .. code-block:: python

            {
                count: Optional[int]          #: total rows matching the query, None if count hasn't been requested
                offset: int                   #: row offset for this selection
                page: Optional[int]           #: current page number, None if count hasn't been requested
                pages: Optional[int]          #: total pages, None if count hasn't been requested
                on_page: int                  #: number of rows on this page
                data: Optional[List[dict]]    #: returned rows, None if limit was set to 0
            }

        """

        if columns and limit:
            sql = self._create_list_query(conditions, sort, offset, limit, columns, session=session)
            coro = self._wrap_get(self._db.fetch(sql))
            if count:
                sql_count = self._create_count_query(conditions, precision, session=session)
                result, count = await asyncio.gather(coro, self._db.fetchval(sql_count))
                page, pages = self._get_page_count(count, offset, limit)
            else:
                result = await coro
                count, page, pages = None, None, None
            on_page = len(result)
        else:
            on_page, result = 0, None
            if count:
                sql_count = self._create_count_query(conditions, precision, session=session)
                count = await self._db.fetchval(sql_count)
                page, pages = self._get_page_count(count, offset, limit)
            else:
                count, page, pages, on_page = None, None, None, 0

        return {
            'count': count,
            'offset': offset,
            'page': page,
            'pages': pages,
            'on_page': on_page,
            'data': result
        }

    async def iter(
            self, conditions=None, sort=None, offset=0, limit=DEFAULT_ROW_LIMIT,
            columns='*', session=ServerSessionFlag) -> AsyncGenerator[List[dict], None]:
        """
        Almost same as `SQLService.list` but returns a generator which iterates
        over the query content. It's not intended to be used by a client
        but inside the app or the service itself.

        See `SQLService.list` for info about params.
        """

        if not sort:
            sort = self._primary_key_list

        result = await self.list(
            conditions=conditions, sort=sort, offset=offset, limit=limit,
            columns=columns, count=True, session=session)

        page, pages = result['page'], result['pages']
        yield result['data']

        page += 1

        while page <= pages:
            offset = (page - 1) * limit
            result = await self.list(
                conditions=conditions, sort=sort, offset=offset, limit=limit,
                columns=columns, count=False, session=session)
            yield result['data']
            page += 1
