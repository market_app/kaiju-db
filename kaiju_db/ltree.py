from sqlalchemy_utils import Ltree as PGLtree


class Ltree(PGLtree):
    """
    Postgresql Ltree validation fix.
    """

    @classmethod
    def validate(cls, path):
        pass
