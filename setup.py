import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


with open(os.path.join(here, 'requirements.txt')) as f:
    req_lines = [line.strip() for line in f.readlines()]

    def fix_url_line(line):
        if '#egg=' in line:
            return line.rsplit('#egg=', 1)[1]
        return line
    requires = [fix_url_line(line) for line in req_lines if line]


setup(
    name='kaiju_db',
    version='0.1.0',
    description='Kaiju database tools, connectors and base classes.',
    long_description=README + '\n\n' + CHANGES,
    long_description_content_type='text/markdown; charset=UTF-8',
    author='anton.nidhoggr',
    author_email='antonnidhoggr@me.com',
    url='https://bitbucket.org/market_app/kaiju-db',
    packages=find_packages(),
    package_dir={'kaiju_db': 'kaiju_db'},
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=requires,
    license='LICENSE.txt',
    zip_safe=False,
    classifiers=(
        'Programming Language :: Python :: 3.8',
    ),
    keywords=['postgres', 'sqlalchemy', 'elemento', 'kaiju']
)
